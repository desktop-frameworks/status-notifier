/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierInterface specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierInterface/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 *
 * Impl Class: Performs all DBus Actions Asynchronously.
 **/

#pragma once

#include <functional>
#include <QObject>
#include <QString>
#include <QtDBus>
#include <QMenu>
#include <QIcon>

#include <DFSNITypes.hpp>

class StatusNotifierInterface;
class StatusNotifierAdaptor;
class DBusMenuExporter;

template<typename> struct remove_class_type { using type = void; };

template<typename C, typename R, typename ... ArgTypes> struct  remove_class_type<R (C::*) ( ArgTypes... )> { using type = R( ArgTypes... ); };

template<typename C, typename R, typename ... ArgTypes> struct remove_class_type<R (C::*) ( ArgTypes... ) const> { using type = R( ArgTypes... ); };

template<typename L> class call_sig_helper {
    template<typename L1>
    static decltype(&L1::operator()) test( int );

    template<typename L1>
    static void test( ... );   //bluff

    public:
        using type = decltype(test<L>( 0 ) );
};

template<typename L> struct call_signature : public remove_class_type<typename call_sig_helper<L>::type> {};

template<typename R, typename ... ArgTypes> struct call_signature<R (ArgTypes...)> { using type = R( ArgTypes... ); };

template<typename R, typename ... ArgTypes> struct call_signature<R (*) ( ArgTypes... )> { using type = R( ArgTypes... ); };

template<typename C, typename R, typename ... ArgTypes> struct call_signature<R (C::*) ( ArgTypes... )> { using type = R( ArgTypes... ); };

template<typename C, typename R, typename ... ArgTypes> struct call_signature<R (C::*) ( ArgTypes... ) const>  { using type = R( ArgTypes... ); };

template<typename> struct is_valid_signature : public std::false_type {};

template<typename Arg> struct is_valid_signature<void (Arg)> : public std::true_type {};

namespace DFL {
    namespace SNI {
        class Server;
        class Client;
    }
}

class DFL::SNI::Client : public QObject {
    Q_OBJECT;

    public:

        /**
         * Initialize the SNI Client object
         */
        Client( QString, QString, QObject *parent = nullptr );

        /**
         * Destroy the SNI Client object
         */
        ~Client();

        template<typename F>
        void propertyGetAsync( QString const& name, F finished ) {
            static_assert(
                is_valid_signature<typename call_signature<F>::type>::value, "need callable ( lambda, *function, callable obj ) ( Arg ) -> void"
            );

            connect(
                new QDBusPendingCallWatcher { asyncPropGet( name ), this },
                &QDBusPendingCallWatcher::finished,
                [ this, finished, name ]( QDBusPendingCallWatcher *call ) {
                    QDBusPendingReply<QVariant> reply = *call;

                    if ( reply.isError() ) {
                        // qWarning() << "Error on DBus request:" << reply.error();
                    }

                    finished( qdbus_cast<typename std::function<typename call_signature<F>::type>::argument_type>( reply.value() ) );
                    call->deleteLater();
                }
            );
        }

        // exposed methods from StatusNotifierInterface
        QString service() const;

    public slots:
        // Forwarded slots from StatusNotifierInterface
        QDBusPendingReply<> Activate( int x, int y );
        QDBusPendingReply<> ContextMenu( int x, int y );
        QDBusPendingReply<> Scroll( int delta, const QString& orientation );
        QDBusPendingReply<> SecondaryActivate( int x, int y );

    signals:
        // Forwarded signals from StatusNotifierInterface
        void NewAttentionIcon();
        void NewIcon();
        void NewOverlayIcon();
        void NewStatus( const QString& status );
        void NewTitle();
        void NewToolTip();

    private:
        QDBusPendingReply<QDBusVariant> asyncPropGet( QString const& property );

    private:
        StatusNotifierInterface *mSni;
};


class DFL::SNI::Server : public QObject {
    Q_OBJECT

    Q_PROPERTY( QString Category READ category )
    Q_PROPERTY( QString Title READ title )
    Q_PROPERTY( QString Id READ id )
    Q_PROPERTY( QString Status READ status )
    Q_PROPERTY( QDBusObjectPath Menu READ menu )

    Q_PROPERTY( QString IconName READ iconName )
    Q_PROPERTY( IconPixmapList IconPixmap READ iconPixmap )

    Q_PROPERTY( QString OverlayIconName READ overlayIconName )
    Q_PROPERTY( IconPixmapList OverlayIconPixmap READ overlayIconPixmap )

    Q_PROPERTY( QString AttentionIconName READ attentionIconName )
    Q_PROPERTY( IconPixmapList AttentionIconPixmap READ attentionIconPixmap )

    Q_PROPERTY( ToolTip ToolTip READ toolTip )

    public:

        /**
         * Initialize the SNI Server object
         */
        Server( QString id, QObject *parent = nullptr );

        /**
         * Initialize the SNI Server object
         */
        ~Server() override;

        QString id() const
        { return mId; }

        QString title() const
        { return mTitle; }
        void setTitle( const QString& title );

        QString status() const
        { return mStatus; }
        void setStatus( const QString& status );

        QString category() const
        { return mCategory; }
        void setCategory( const QString& category );

        QDBusObjectPath menu() const
        { return mMenuPath; }
        void setMenuPath( const QString& path );

        QString iconName() const
        { return mIconName; }
        void setIconByName( const QString& name );

        IconPixmapList iconPixmap() const
        { return mIcon; }
        void setIconByPixmap( const QIcon& icon );

        QString overlayIconName() const
        { return mOverlayIconName; }
        void setOverlayIconByName( const QString& name );

        IconPixmapList overlayIconPixmap() const
        { return mOverlayIcon; }
        void setOverlayIconByPixmap( const QIcon& icon );

        QString attentionIconName() const
        { return mAttentionIconName; }
        void setAttentionIconByName( const QString& name );

        IconPixmapList attentionIconPixmap() const
        { return mAttentionIcon; }
        void setAttentionIconByPixmap( const QIcon& icon );

        QString toolTipTitle() const
        { return mTooltipTitle; }
        void setToolTipTitle( const QString& title );

        QString toolTipSubTitle() const
        { return mTooltipSubtitle; }
        void setToolTipSubTitle( const QString& subTitle );

        QString toolTipIconName() const
        { return mTooltipIconName; }
        void setToolTipIconByName( const QString& name );

        IconPixmapList toolTipIconPixmap() const
        { return mTooltipIcon; }
        void setToolTipIconByPixmap( const QIcon& icon );

        ToolTip toolTip() const {
            ToolTip tt;

            tt.title       = mTooltipTitle;
            tt.description = mTooltipSubtitle;
            tt.iconName    = mTooltipIconName;
            tt.iconPixmap  = mTooltipIcon;
            return tt;
        }

        /*!
         * \Note: we don't take ownership for the \param menu
         */
        void setContextMenu( QMenu *menu );

    public Q_SLOTS:
        void Activate( int x, int y );
        void SecondaryActivate( int x, int y );
        void ContextMenu( int x, int y );
        void Scroll( int delta, const QString& orientation );

        void showMessage( const QString& title, const QString& msg, const QString& iconName, int secs );

    private:
        void registerToHost();
        IconPixmapList iconToPixmapList( const QIcon& icon );

    private Q_SLOTS:
        void onServiceOwnerChanged( const QString& service, const QString& oldOwner, const QString& newOwner );
        void onMenuDestroyed();

    Q_SIGNALS:
        void activateRequested( const QPoint& pos );
        void secondaryActivateRequested( const QPoint& pos );
        void scrollRequested( int delta, Qt::Orientation orientation );

    private:
        StatusNotifierAdaptor *mAdaptor;

        QString mService;
        QString mId;
        QString mTitle;
        QString mStatus;
        QString mCategory;

        // icons
        QString mIconName, mOverlayIconName, mAttentionIconName;
        IconPixmapList mIcon, mOverlayIcon, mAttentionIcon;
        qint64 mIconCacheKey, mOverlayIconCacheKey, mAttentionIconCacheKey;

        // tooltip
        QString mTooltipTitle, mTooltipSubtitle, mTooltipIconName;
        IconPixmapList mTooltipIcon;
        qint64 mTooltipIconCacheKey;

        // menu
        QMenu *mMenu;
        QDBusObjectPath mMenuPath;
        DBusMenuExporter *mMenuExporter;
        QDBusConnection mSessionBus;

        static int mServiceCounter;
};

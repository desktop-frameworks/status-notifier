/* BEGIN_COMMON_COPYRIGHT_HEADER
* (c)LGPL2+
*
* LXQt - a lightweight, Qt based, desktop toolset
* https://lxqt.org/
*
* Copyright: 2015 LXQt team
* Authors:
*   Paulo Lieuthier <paulolieuthier@gmail.com>
*
* This program or library is free software; you can redistribute it
* and/or modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General
* Public License along with this library; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*
* END_COMMON_COPYRIGHT_HEADER */

#include "DFStatusNotifierItem.hpp"
#include "SNIAdaptor.hpp"

#include <QDBusInterface>
#include <QDBusServiceWatcher>
#include <utility>
#include <dbusmenuexporter.h>

int DFL::SNI::Server::mServiceCounter = 0;

DFL::SNI::Server::Server( QString id, QObject *parent ) : QObject( parent ),
    mAdaptor( new StatusNotifierAdaptor( this ) ),
    mService( QString::fromLatin1( "org.freedesktop.StatusNotifierItem-%1-%2" )
                 .arg( QCoreApplication::applicationPid() )
                 .arg( ++mServiceCounter ) ),
    mId( std::move( id ) ),
    mTitle( QLatin1String( "Test" ) ),
    mStatus( QLatin1String( "Active" ) ),
    mCategory( QLatin1String( "ApplicationStatus" ) ),
    mMenu( nullptr ),
    mMenuPath( QLatin1String( "/NO_DBUSMENU" ) ),
    mMenuExporter( nullptr ),
    mSessionBus( QDBusConnection::connectToBus( QDBusConnection::SessionBus, mService ) ) {
    mSessionBus.registerObject( QLatin1String( "/StatusNotifierItem" ), this );

    registerToHost();

    qRegisterMetaType<DFL::SNI::IconPixmap>( "IconPixmap" );
    qDBusRegisterMetaType<DFL::SNI::IconPixmap>();

    qRegisterMetaType<DFL::SNI::IconPixmapList>( "IconPixmapList" );
    qDBusRegisterMetaType<DFL::SNI::IconPixmapList>();

    qRegisterMetaType<DFL::SNI::ToolTip>( "ToolTip" );
    qDBusRegisterMetaType<DFL::SNI::ToolTip>();

    // monitor the watcher service in case the host restarts
    QDBusServiceWatcher *watcher = new QDBusServiceWatcher(
        QLatin1String( "org.kde.StatusNotifierWatcher" ),
        mSessionBus,
        QDBusServiceWatcher::WatchForOwnerChange,
        this
    );

    connect( watcher, &QDBusServiceWatcher::serviceOwnerChanged, this, &DFL::SNI::Server::onServiceOwnerChanged );
}


DFL::SNI::Server::~Server() {
    mSessionBus.unregisterObject( QLatin1String( "/StatusNotifierItem" ) );
    QDBusConnection::disconnectFromBus( mService );
}


void DFL::SNI::Server::registerToHost() {
    QDBusInterface interface(
        QLatin1String( "org.kde.StatusNotifierWatcher" ),
        QLatin1String( "/StatusNotifierWatcher" ),
        QLatin1String( "org.kde.StatusNotifierWatcher" ),
        mSessionBus
    );

    interface.asyncCall( QLatin1String( "RegisterStatusNotifierItem" ), mSessionBus.baseService() );
}


void DFL::SNI::Server::onServiceOwnerChanged( const QString& service, const QString& oldOwner, const QString& newOwner ) {
    Q_UNUSED( service );
    Q_UNUSED( oldOwner );

    if ( !newOwner.isEmpty() ) {
        registerToHost();
    }
}


void DFL::SNI::Server::onMenuDestroyed() {
    mMenu = nullptr;
    setMenuPath( QLatin1String( "/NO_DBUSMENU" ) );
    mMenuExporter = nullptr; //mMenu is a QObject parent of the mMenuExporter
}


void DFL::SNI::Server::setTitle( const QString& title ) {
    if ( mTitle == title ) {
        return;
    }

    mTitle = title;
    Q_EMIT mAdaptor->NewTitle();
}


void DFL::SNI::Server::setStatus( const QString& status ) {
    if ( mStatus == status ) {
        return;
    }

    mStatus = status;
    Q_EMIT mAdaptor->NewStatus( mStatus );
}


void DFL::SNI::Server::setCategory( const QString& category ) {
    if ( mCategory == category ) {
        return;
    }

    mCategory = category;
}


void DFL::SNI::Server::setMenuPath( const QString& path ) {
    mMenuPath.setPath( path );
}


void DFL::SNI::Server::setIconByName( const QString& name ) {
    if ( mIconName == name ) {
        return;
    }

    mIconName = name;
    Q_EMIT mAdaptor->NewIcon();
}


void DFL::SNI::Server::setIconByPixmap( const QIcon& icon ) {
    mIconCacheKey = icon.cacheKey();
    mIcon         = iconToPixmapList( icon );
    mIconName.clear();
    Q_EMIT mAdaptor->NewIcon();
}


void DFL::SNI::Server::setOverlayIconByName( const QString& name ) {
    if ( mOverlayIconName == name ) {
        return;
    }

    mOverlayIconName = name;
    Q_EMIT mAdaptor->NewOverlayIcon();
}


void DFL::SNI::Server::setOverlayIconByPixmap( const QIcon& icon ) {
    mOverlayIconCacheKey = icon.cacheKey();
    mOverlayIcon         = iconToPixmapList( icon );
    mOverlayIconName.clear();
    Q_EMIT mAdaptor->NewOverlayIcon();
}


void DFL::SNI::Server::setAttentionIconByName( const QString& name ) {
    if ( mAttentionIconName == name ) {
        return;
    }

    mAttentionIconName = name;
    Q_EMIT mAdaptor->NewAttentionIcon();
}


void DFL::SNI::Server::setAttentionIconByPixmap( const QIcon& icon ) {
    if ( mAttentionIconCacheKey == icon.cacheKey() ) {
        return;
    }

    mAttentionIconCacheKey = icon.cacheKey();
    mAttentionIcon         = iconToPixmapList( icon );
    mAttentionIconName.clear();
    Q_EMIT mAdaptor->NewAttentionIcon();
}


void DFL::SNI::Server::setToolTipTitle( const QString& title ) {
    if ( mTooltipTitle == title ) {
        return;
    }

    mTooltipTitle = title;
    Q_EMIT mAdaptor->NewToolTip();
}


void DFL::SNI::Server::setToolTipSubTitle( const QString& subTitle ) {
    if ( mTooltipSubtitle == subTitle ) {
        return;
    }

    mTooltipSubtitle = subTitle;
    Q_EMIT mAdaptor->NewToolTip();
}


void DFL::SNI::Server::setToolTipIconByName( const QString& name ) {
    if ( mTooltipIconName == name ) {
        return;
    }

    mTooltipIconName = name;
    Q_EMIT mAdaptor->NewToolTip();
}


void DFL::SNI::Server::setToolTipIconByPixmap( const QIcon& icon ) {
    mTooltipIconCacheKey = icon.cacheKey();
    mTooltipIcon         = iconToPixmapList( icon );
    mTooltipIconName.clear();
    Q_EMIT mAdaptor->NewToolTip();
}


void DFL::SNI::Server::setContextMenu( QMenu *menu ) {
    if ( mMenu == menu ) {
        return;
    }

    if ( nullptr != mMenu ) {
        disconnect( mMenu, &QObject::destroyed, this, &DFL::SNI::Server::onMenuDestroyed );
    }

    mMenu = menu;

    if ( nullptr != mMenu ) {
        setMenuPath( QLatin1String( "/MenuBar" ) );
    }
    else {
        setMenuPath( QLatin1String( "/NO_DBUSMENU" ) );
    }

    //Note: we need to destroy menu exporter before creating new one -> to free the DBus object path for new
    // menu
    delete mMenuExporter;

    if ( nullptr != mMenu ) {
        connect( mMenu, &QObject::destroyed, this, &DFL::SNI::Server::onMenuDestroyed );
        mMenuExporter = new DBusMenuExporter{ this->menu().path(), mMenu, mSessionBus };
    }
}


void DFL::SNI::Server::Activate( int x, int y ) {
    if ( mStatus == QLatin1String( "NeedsAttention" ) ) {
        mStatus = QLatin1String( "Active" );
    }

    Q_EMIT activateRequested( QPoint( x, y ) );
}


void DFL::SNI::Server::SecondaryActivate( int x, int y ) {
    if ( mStatus == QLatin1String( "NeedsAttention" ) ) {
        mStatus = QLatin1String( "Active" );
    }

    Q_EMIT secondaryActivateRequested( QPoint( x, y ) );
}


void DFL::SNI::Server::ContextMenu( int x, int y ) {
    if ( mMenu != nullptr ) {
        if ( mMenu->isVisible() ) {
            mMenu->popup( QPoint( x, y ) );
        }
        else {
            mMenu->hide();
        }
    }
}


void DFL::SNI::Server::Scroll( int delta, const QString& orientation ) {
    Qt::Orientation orient = Qt::Vertical;

    if ( orientation.toLower() == QLatin1String( "horizontal" ) ) {
        orient = Qt::Horizontal;
    }

    Q_EMIT scrollRequested( delta, orient );
}


void DFL::SNI::Server::showMessage( const QString& title, const QString& msg, const QString& iconName, int secs ) {
    QDBusInterface interface( QLatin1String( "org.freedesktop.Notifications" ), QLatin1String( "/org/freedesktop/Notifications" ),
                              QLatin1String( "org.freedesktop.Notifications" ), mSessionBus );

    interface.call( QLatin1String( "Notify" ), mTitle, (uint)0, iconName, title,
                    msg, QStringList(), QVariantMap(), secs );
}


DFL::SNI::IconPixmapList DFL::SNI::Server::iconToPixmapList( const QIcon& icon ) {
    DFL::SNI::IconPixmapList pixmapList;

    // long live KDE!
    QList<QSize> sizes = icon.availableSizes();

    /**
     * Some, like qbittorrent, do not return any size, but they have valid icon none-the-less
     * So, if no sizes are available, we will attempt to generate a few sizes: 16, 24, 32, 48
     */
    if ( sizes.isEmpty() ) {
        sizes << QSize( 16, 16 );
        sizes << QSize( 24, 24 );
        sizes << QSize( 32, 32 );
        sizes << QSize( 48, 48 );
    }

    for (const QSize& size : sizes) {
        QImage image = icon.pixmap( size ).toImage();

        IconPixmap pix;
        pix.height = image.height();
        pix.width  = image.width();

        if ( image.format() != QImage::Format_ARGB32 ) {
            image = image.convertToFormat( QImage::Format_ARGB32 );
        }

        pix.bytes = QByteArray( (char *)image.bits(), image.sizeInBytes() );

        // swap to network byte order if we are little endian
        if ( QSysInfo::ByteOrder == QSysInfo::LittleEndian ) {
            quint32 *uintBuf = (quint32 *)pix.bytes.data();
            for (uint i = 0; i < pix.bytes.size() / sizeof(quint32); ++i) {
                *uintBuf = qToBigEndian( *uintBuf );
                ++uintBuf;
            }
        }

        pixmapList.append( pix );
    }

    return pixmapList;
}

#include "SNIAdaptor.hpp"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

#include "DFSNITypes.hpp"

/*
 * Implementation of adaptor class StatusNotifierAdaptor
 */

StatusNotifierAdaptor::StatusNotifierAdaptor( DFL::SNI::Server *parent ) : QDBusAbstractAdaptor( parent ) {
    // constructor
    setAutoRelaySignals( true );
}


StatusNotifierAdaptor::~StatusNotifierAdaptor() {
    // destructor
}


QString StatusNotifierAdaptor::attentionIconName() const {
    // get the value of property AttentionIconName
    return qvariant_cast<QString>( parent()->property( "AttentionIconName" ) );
}


DFL::SNI::IconPixmapList StatusNotifierAdaptor::attentionIconPixmap() const {
    // get the value of property AttentionIconPixmap
    return qvariant_cast<DFL::SNI::IconPixmapList>( parent()->property( "AttentionIconPixmap" ) );
}


QString StatusNotifierAdaptor::attentionMovieName() const {
    // get the value of property AttentionMovieName
    return qvariant_cast<QString>( parent()->property( "AttentionMovieName" ) );
}


QString StatusNotifierAdaptor::category() const {
    // get the value of property Category
    return qvariant_cast<QString>( parent()->property( "Category" ) );
}


QString StatusNotifierAdaptor::iconName() const {
    // get the value of property IconName
    return qvariant_cast<QString>( parent()->property( "IconName" ) );
}


DFL::SNI::IconPixmapList StatusNotifierAdaptor::iconPixmap() const {
    // get the value of property IconPixmap
    return qvariant_cast<DFL::SNI::IconPixmapList>( parent()->property( "IconPixmap" ) );
}


QString StatusNotifierAdaptor::iconThemePath() const {
    // get the value of property IconThemePath
    return qvariant_cast<QString>( parent()->property( "IconThemePath" ) );
}


QString StatusNotifierAdaptor::id() const {
    // get the value of property Id
    return qvariant_cast<QString>( parent()->property( "Id" ) );
}


bool StatusNotifierAdaptor::itemIsMenu() const {
    // get the value of property ItemIsMenu
    return qvariant_cast<bool>( parent()->property( "ItemIsMenu" ) );
}


QDBusObjectPath StatusNotifierAdaptor::menu() const {
    // get the value of property Menu
    return qvariant_cast<QDBusObjectPath>( parent()->property( "Menu" ) );
}


QString StatusNotifierAdaptor::overlayIconName() const {
    // get the value of property OverlayIconName
    return qvariant_cast<QString>( parent()->property( "OverlayIconName" ) );
}


DFL::SNI::IconPixmapList StatusNotifierAdaptor::overlayIconPixmap() const {
    // get the value of property OverlayIconPixmap
    return qvariant_cast<DFL::SNI::IconPixmapList>( parent()->property( "OverlayIconPixmap" ) );
}


QString StatusNotifierAdaptor::status() const {
    // get the value of property Status
    return qvariant_cast<QString>( parent()->property( "Status" ) );
}


QString StatusNotifierAdaptor::title() const {
    // get the value of property Title
    return qvariant_cast<QString>( parent()->property( "Title" ) );
}


DFL::SNI::ToolTip StatusNotifierAdaptor::toolTip() const {
    // get the value of property ToolTip
    return qvariant_cast<DFL::SNI::ToolTip>( parent()->property( "ToolTip" ) );
}


int StatusNotifierAdaptor::windowId() const {
    // get the value of property WindowId
    return qvariant_cast<int>( parent()->property( "WindowId" ) );
}


void StatusNotifierAdaptor::Activate( int x, int y ) {
    // handle method call org.kde.StatusNotifierItem.Activate
    parent()->Activate( x, y );
}


void StatusNotifierAdaptor::ContextMenu( int x, int y ) {
    // handle method call org.kde.StatusNotifierItem.ContextMenu
    parent()->ContextMenu( x, y );
}


void StatusNotifierAdaptor::Scroll( int delta, const QString& orientation ) {
    // handle method call org.kde.StatusNotifierItem.Scroll
    parent()->Scroll( delta, orientation );
}


void StatusNotifierAdaptor::SecondaryActivate( int x, int y ) {
    // handle method call org.kde.StatusNotifierItem.SecondaryActivate
    parent()->SecondaryActivate( x, y );
}


#include "SNIAdaptor.moc"

/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include "DFSNITypes.hpp"
#include "DFStatusNotifierWatcher.hpp"

#include "StatusNotifierWatcherImpl.hpp"


DFL::StatusNotifierWatcher * DFL::StatusNotifierWatcher::createInstance( QString service ) {
    if ( service == "kde" ) {
        return new DFL::Impl::KDEStatusNotifierWatcher();
    }

    else if ( service == "freedesktop" ) {
        return new DFL::Impl::FDOStatusNotifierWatcher();
    }

    return nullptr;
}


DFL::StatusNotifierWatcher::StatusNotifierWatcher( QString service ) : QObject() {
    qRegisterMetaType<DFL::SNI::IconPixmap>( "IconPixmap" );
    qDBusRegisterMetaType<DFL::SNI::IconPixmap>();

    qRegisterMetaType<DFL::SNI::IconPixmapList>( "IconPixmapList" );
    qDBusRegisterMetaType<DFL::SNI::IconPixmapList>();

    qRegisterMetaType<DFL::SNI::ToolTip>( "ToolTip" );
    qDBusRegisterMetaType<DFL::SNI::ToolTip>();

    mWatcherServiceName = QString( "org.%1.StatusNotifierWatcher" ).arg( service );
}


DFL::StatusNotifierWatcher::~StatusNotifierWatcher() {
    QDBusConnection::sessionBus().unregisterService( mWatcherServiceName );

    if ( mWatcher != nullptr ) {
        delete mWatcher;
        mWatcher = nullptr;
    }
}


bool DFL::StatusNotifierWatcher::isStatusNotifierHostRegistered() {
    return mHosts.count() > 0;
}


int DFL::StatusNotifierWatcher::protocolVersion() const {
    return 0;
}


QStringList DFL::StatusNotifierWatcher::RegisteredStatusNotifierItems() const {
    return mServices;
}

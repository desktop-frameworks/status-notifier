/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#pragma once

#include <QDBusArgument>
#include <QtCore>

namespace DFL {
    namespace SNI {
        struct IconPixmap {
            int        width;
            int        height;
            QByteArray bytes;
        };

        typedef QList<IconPixmap> IconPixmapList;

        struct ToolTip {
            QString           iconName;
            QList<IconPixmap> iconPixmap;
            QString           title;
            QString           description;
        };
    }
}

Q_DECLARE_METATYPE( DFL::SNI::IconPixmapList );
Q_DECLARE_METATYPE( DFL::SNI::IconPixmap );
Q_DECLARE_METATYPE( DFL::SNI::ToolTip );

QDBusArgument &operator<<( QDBusArgument& argument, const DFL::SNI::IconPixmap& icon );
const QDBusArgument &operator>>( const QDBusArgument& argument, DFL::SNI::IconPixmap& icon );

QDBusArgument &operator<<( QDBusArgument& argument, const DFL::SNI::ToolTip& toolTip );
const QDBusArgument &operator>>( const QDBusArgument& argument, DFL::SNI::ToolTip& toolTip );

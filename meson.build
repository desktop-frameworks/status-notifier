project(
	'DFL SNI',
	'cpp',
	version: '0.2.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DPROJECT_VERSION=' + meson.project_version(), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

if get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )
	QtDeps    = dependency( 'qt5', modules: [ 'Core', 'Gui', 'Widgets', 'DBus' ] )
	DBusMenu  = dependency( 'dbusmenu-qt5' )

	libname   = 'df5sni'
	subdirname = 'DFL/DF5'

elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )
	QtDeps    = dependency( 'qt6', modules: [ 'Core', 'Gui', 'Widgets', 'DBus' ] )
	DBusMenu  = dependency( 'dbusmenu-lxqt' )

	libname   = 'df6sni'
	subdirname = 'DFL/DF6'

endif

Headers = [
	'SNIInterface.hpp',
	'SNIAdaptor.hpp',
	'DFSNITypes.hpp',
	'DFStatusNotifierItem.hpp',
	'DFStatusNotifierWatcher.hpp',
	'StatusNotifierWatcherImpl.hpp'
]

Sources = [
	'SNIInterface.cpp',
	'SNIAdaptor.cpp',
	'SNITypes.cpp',
	'SNIClient.cpp',
	'SNIServer.cpp',
	'StatusNotifierWatcher.cpp',
	'StatusNotifierWatcherImpl.cpp'
]

Mocs = Qt.compile_moc(
 	headers : [ Headers ],
	sources : [ 'SNIAdaptor.cpp' ],
 	dependencies: [QtDeps, DBusMenu],
)

sni = shared_library(
    libname, [ Sources, Mocs ],
	version: meson.project_version(),
 	dependencies: [QtDeps, DBusMenu],
    install: true,
    install_dir: join_paths( get_option( 'libdir' ) )
)

install_headers( [ 'DFStatusNotifierWatcher.hpp', 'DFStatusNotifierItem.hpp', 'DFSNITypes.hpp' ], subdir: subdirname )

summary = [
	'',
	'----------------',
	'DFL::SNI @0@'.format( meson.project_version() ),
	'----------------',
	''
]
message( '\n'.join( summary ) )

## PkgConfig Section
pkgconfig = import( 'pkgconfig' )
pkgconfig.generate(
	sni,
	name: libname,
	subdirs: subdirname,
	version: meson.project_version(),
	filebase: libname,
	description: 'XDG SNI Specs Implementation',
)

/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include "DFSNITypes.hpp"
#include "DFStatusNotifierWatcher.hpp"

#include "StatusNotifierWatcherImpl.hpp"


DFL::Impl::KDEStatusNotifierWatcher::KDEStatusNotifierWatcher() : DFL::StatusNotifierWatcher( "kde" ) {
    QDBusConnection kdeDBus = QDBusConnection::connectToBus( QDBusConnection::SessionBus, "KDEConnection" );

    serviceRunning   = kdeDBus.registerService( "org.kde.StatusNotifierWatcher" );
    objectRegistered = kdeDBus.registerObject( "/StatusNotifierWatcher", this, QDBusConnection::ExportAdaptors | QDBusConnection::ExportScriptableContents );

    mWatcher = new QDBusServiceWatcher( this );
    mWatcher->setConnection( kdeDBus );
    mWatcher->setWatchMode( QDBusServiceWatcher::WatchForUnregistration );

    connect( mWatcher, &QDBusServiceWatcher::serviceUnregistered, this, &DFL::Impl::KDEStatusNotifierWatcher::serviceUnregistered );
}


bool DFL::Impl::KDEStatusNotifierWatcher::isServiceRunning() {
    return serviceRunning;
}


bool DFL::Impl::KDEStatusNotifierWatcher::isObjectRegistered() {
    return objectRegistered;
}


void DFL::Impl::KDEStatusNotifierWatcher::RegisterStatusNotifierItem( const QString& serviceOrPath ) {
    QString service = serviceOrPath;
    QString path    = "/StatusNotifierItem";

    // workaround for sni-qt
    if ( service.startsWith( '/' ) ) {
        path    = service;
        service = message().service();
    }

    QString notifierItemId = service + path;

    if ( QDBusConnection::sessionBus().interface()->isServiceRegistered( service ).value() && !mServices.contains( notifierItemId ) ) {
        mServices << notifierItemId;
        mWatcher->addWatchedService( service );
        emit StatusNotifierItemRegistered( notifierItemId );
    }
}


void DFL::Impl::KDEStatusNotifierWatcher::RegisterStatusNotifierHost( const QString& service ) {
    if ( !mHosts.contains( service ) ) {
        mHosts.append( service );
        mWatcher->addWatchedService( service );

        emit StatusNotifierHostRegistered();
    }
}


void DFL::Impl::KDEStatusNotifierWatcher::serviceUnregistered( const QString& service ) {
    mWatcher->removeWatchedService( service );

    if ( mHosts.contains( service ) ) {
        mHosts.removeAll( service );
        return;
    }

    QString               match = service + '/';
    QStringList::Iterator it    = mServices.begin();

    while ( it != mServices.end() ) {
        if ( it->startsWith( match ) ) {
            QString name = *it;
            it = mServices.erase( it );
            emit StatusNotifierItemUnregistered( name );
        }

        else {
            ++it;
        }
    }
}


DFL::Impl::FDOStatusNotifierWatcher::FDOStatusNotifierWatcher() : DFL::StatusNotifierWatcher( "freedesktop" ) {
    QDBusConnection fdoDBus = QDBusConnection::connectToBus( QDBusConnection::SessionBus, "FDOConnection" );

    serviceRunning   = fdoDBus.registerService( "org.freedesktop.StatusNotifierWatcher" );
    objectRegistered = fdoDBus.registerObject( "/StatusNotifierWatcher", this, QDBusConnection::ExportAdaptors | QDBusConnection::ExportScriptableContents );

    mWatcher = new QDBusServiceWatcher( this );
    mWatcher->setConnection( fdoDBus );
    mWatcher->setWatchMode( QDBusServiceWatcher::WatchForUnregistration );

    connect( mWatcher, &QDBusServiceWatcher::serviceUnregistered, this, &DFL::Impl::FDOStatusNotifierWatcher::serviceUnregistered );
}


bool DFL::Impl::FDOStatusNotifierWatcher::isServiceRunning() {
    return serviceRunning;
}


bool DFL::Impl::FDOStatusNotifierWatcher::isObjectRegistered() {
    return objectRegistered;
}


void DFL::Impl::FDOStatusNotifierWatcher::RegisterStatusNotifierItem( const QString& serviceOrPath ) {
    QString service = serviceOrPath;
    QString path    = "/StatusNotifierItem";

    // workaround for sni-qt
    if ( service.startsWith( '/' ) ) {
        path    = service;
        service = message().service();
    }

    QString notifierItemId = service + path;

    if ( QDBusConnection::sessionBus().interface()->isServiceRegistered( service ).value() && !mServices.contains( notifierItemId ) ) {
        mServices << notifierItemId;
        mWatcher->addWatchedService( service );
        emit StatusNotifierItemRegistered( notifierItemId );
    }
}


void DFL::Impl::FDOStatusNotifierWatcher::RegisterStatusNotifierHost( const QString& service ) {
    if ( !mHosts.contains( service ) ) {
        mHosts.append( service );
        mWatcher->addWatchedService( service );

        emit StatusNotifierHostRegistered();
    }
}


void DFL::Impl::FDOStatusNotifierWatcher::serviceUnregistered( const QString& service ) {
    mWatcher->removeWatchedService( service );

    if ( mHosts.contains( service ) ) {
        mHosts.removeAll( service );
        return;
    }

    QString               match = service + '/';
    QStringList::Iterator it    = mServices.begin();

    while ( it != mServices.end() ) {
        if ( it->startsWith( match ) ) {
            QString name = *it;
            it = mServices.erase( it );
            emit StatusNotifierItemUnregistered( name );
        }

        else {
            ++it;
        }
    }
}

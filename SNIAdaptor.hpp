#pragma once

#include <QtCore/QObject>
#include <QtDBus/QtDBus>
#include "DFStatusNotifierItem.hpp"

#include <QVariant>
#include <QByteArray>
#include <QList>
#include <QMap>
#include <QString>
#include <QStringList>

#include "DFSNITypes.hpp"

/*
 * Adaptor class for interface org.kde.StatusNotifierItem
 */
class StatusNotifierAdaptor : public QDBusAbstractAdaptor {
    Q_OBJECT
    Q_CLASSINFO( "D-Bus Interface",     "org.kde.StatusNotifierItem" )
    Q_CLASSINFO( "D-Bus Introspection", ""
                 "  <interface name=\"org.kde.StatusNotifierItem\">\n"
                 "    <property access=\"read\" type=\"s\" name=\"Category\"/>\n"
                 "    <property access=\"read\" type=\"s\" name=\"Id\"/>\n"
                 "    <property access=\"read\" type=\"s\" name=\"Title\"/>\n"
                 "    <property access=\"read\" type=\"s\" name=\"Status\"/>\n"
                 "    <property access=\"read\" type=\"i\" name=\"WindowId\"/>\n"
                 "    <property access=\"read\" type=\"s\" name=\"IconThemePath\"/>\n"
                 "    <property access=\"read\" type=\"o\" name=\"Menu\"/>\n"
                 "    <property access=\"read\" type=\"b\" name=\"ItemIsMenu\"/>\n"
                 "    <property access=\"read\" type=\"s\" name=\"IconName\"/>\n"
                 "    <property access=\"read\" type=\"a(iiay)\" name=\"IconPixmap\">\n"
                 "      <annotation value=\"IconPixmapList\" name=\"org.qtproject.QtDBus.QtTypeName\"/>\n"
                 "    </property>\n"
                 "    <property access=\"read\" type=\"s\" name=\"OverlayIconName\"/>\n"
                 "    <property access=\"read\" type=\"a(iiay)\" name=\"OverlayIconPixmap\">\n"
                 "      <annotation value=\"IconPixmapList\" name=\"org.qtproject.QtDBus.QtTypeName\"/>\n"
                 "    </property>\n"
                 "    <property access=\"read\" type=\"s\" name=\"AttentionIconName\"/>\n"
                 "    <property access=\"read\" type=\"a(iiay)\" name=\"AttentionIconPixmap\">\n"
                 "      <annotation value=\"IconPixmapList\" name=\"org.qtproject.QtDBus.QtTypeName\"/>\n"
                 "    </property>\n"
                 "    <property access=\"read\" type=\"s\" name=\"AttentionMovieName\"/>\n"
                 "    <property access=\"read\" type=\"(sa(iiay)ss)\" name=\"ToolTip\">\n"
                 "      <annotation value=\"ToolTip\" name=\"org.qtproject.QtDBus.QtTypeName\"/>\n"
                 "    </property>\n"
                 "    <method name=\"ContextMenu\">\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"x\"/>\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"y\"/>\n"
                 "    </method>\n"
                 "    <method name=\"Activate\">\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"x\"/>\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"y\"/>\n"
                 "    </method>\n"
                 "    <method name=\"SecondaryActivate\">\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"x\"/>\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"y\"/>\n"
                 "    </method>\n"
                 "    <method name=\"Scroll\">\n"
                 "      <arg direction=\"in\" type=\"i\" name=\"delta\"/>\n"
                 "      <arg direction=\"in\" type=\"s\" name=\"orientation\"/>\n"
                 "    </method>\n"
                 "    <signal name=\"NewTitle\"/>\n"
                 "    <signal name=\"NewIcon\"/>\n"
                 "    <signal name=\"NewAttentionIcon\"/>\n"
                 "    <signal name=\"NewOverlayIcon\"/>\n"
                 "    <signal name=\"NewToolTip\"/>\n"
                 "    <signal name=\"NewStatus\">\n"
                 "      <arg type=\"s\" name=\"status\"/>\n"
                 "    </signal>\n"
                 "  </interface>\n"
                 "" )
    public:
        StatusNotifierAdaptor( DFL::SNI::Server *parent );
        virtual ~StatusNotifierAdaptor();

        inline DFL::SNI::Server *parent() const
        { return static_cast<DFL::SNI::Server *>(QObject::parent() ); }

    public: // PROPERTIES
        Q_PROPERTY( QString AttentionIconName READ attentionIconName )
        QString attentionIconName() const;

        Q_PROPERTY( DFL::SNI::IconPixmapList AttentionIconPixmap READ attentionIconPixmap )
        DFL::SNI::IconPixmapList attentionIconPixmap() const;

        Q_PROPERTY( QString AttentionMovieName READ attentionMovieName )
        QString attentionMovieName() const;

        Q_PROPERTY( QString Category READ category )
        QString category() const;

        Q_PROPERTY( QString IconName READ iconName )
        QString iconName() const;

        Q_PROPERTY( DFL::SNI::IconPixmapList IconPixmap READ iconPixmap )
        DFL::SNI::IconPixmapList iconPixmap() const;

        Q_PROPERTY( QString IconThemePath READ iconThemePath )
        QString iconThemePath() const;

        Q_PROPERTY( QString Id READ id )
        QString id() const;

        Q_PROPERTY( bool ItemIsMenu READ itemIsMenu )
        bool itemIsMenu() const;

        Q_PROPERTY( QDBusObjectPath Menu READ menu )
        QDBusObjectPath menu() const;

        Q_PROPERTY( QString OverlayIconName READ overlayIconName )
        QString overlayIconName() const;

        Q_PROPERTY( DFL::SNI::IconPixmapList OverlayIconPixmap READ overlayIconPixmap )
        DFL::SNI::IconPixmapList overlayIconPixmap() const;

        Q_PROPERTY( QString Status READ status )
        QString status() const;

        Q_PROPERTY( QString Title READ title )
        QString title() const;

        Q_PROPERTY( DFL::SNI::ToolTip ToolTip READ toolTip )
        DFL::SNI::ToolTip toolTip() const;

        Q_PROPERTY( int WindowId READ windowId )
        int windowId() const;

    public Q_SLOTS: // METHODS
        void Activate( int x, int y );
        void ContextMenu( int x, int y );
        void Scroll( int delta, const QString& orientation );
        void SecondaryActivate( int x, int y );

    Q_SIGNALS: // SIGNALS
        void NewAttentionIcon();
        void NewIcon();
        void NewOverlayIcon();
        void NewStatus( const QString& status );
        void NewTitle();
        void NewToolTip();

    private:
};
